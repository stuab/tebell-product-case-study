package com.thomas.bell.targetdemo.interfaces;

import java.util.Optional;

import com.thomas.bell.targetdemo.models.Product;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {

    // public Product getProduct(String _id);
    public Optional<Product> findById(Long id);
    
}
